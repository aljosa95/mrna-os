/**
 * @file mrna-server.c
 * @author Alija-Ali Abadzic 01427575 <e01427575@student.tuwien.ac.at>
 * @date December 2017
 *
 * @brief Main program module.
 * 
 * Program for server-side of mrna porgram that accept sequences
 * of clients mrna and store them or give it's protein sequence that has
 * been found, using shared memory and semaphores for communication.
 **/

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdbool.h>

/**
 * Name of shared memory object
 */
#define SHM_NAME "/01427575_mrna_shm"

/**
 * Permission for shm object
 */
#define PERMISSION (0600)

/**
 * Name of server semaphore 
 */
#define SEM_SERVER "/01427575_sem_server"

/**
 * Name of client semaphore
 */
#define SEM_CLIENT "/01427575_sem_client"

/**
 * Name of global control semaphore
 */
 #define SEM_GLOBAL "/01427575_sem_cntrl"

/**
 * Length of mrna that server receive from client
 */
#define MSG_LEN (100)

/**
 * Length of response that server sneds to client
 */
#define RES_LEN (200)

/**
 * Global variable
 * @brief Pointer to programm name
 */
char *prog_name = "<not_set>";

/**
 * Global id counter, that is being assigned 
 * to every client connected to server
 */
int global_id_counter = 0;

/**
 * Shared memory file descriptor
 */
static int shmfd;

/**
 * Struct store data about each client that is connected to server
 */
typedef struct {

    // client ID
    int ID;

    // save current cursor of sequence 
    int cursor;

    // store sequence message
    char seqeunce[MSG_LEN];

    // store occurance of seq
    bool seq_saved;
} Client;

/**
 * Struct used for share memory
 */
struct shm {

    // client ID
    int ID;

    // 0 - start/begin msg from client, 1 - submit new seq (s)
    // 2 - next protein seq (n)   3 - reset active seq (r) 
    // 4 - close client (q)
    int cmd;

    // sequence msg from client if cmd is 1 (s)
    char seqeunce[MSG_LEN];

    // protein sequence that has been found or other response depend. on cmd
    char server_response[RES_LEN];

};


static struct shm *shared;

/**
 * Mandatory usage function.
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: prog_name
 */
static void usage(){
    (void) fprintf(stderr,"USAGE: %s\n", prog_name);
    exit(EXIT_FAILURE);
}

/**
 * Print out error msg and exit programm with exit_failure
 */
static void bail_out(char *msg){
    (void)fprintf(stderr, "%s: %s\n",prog_name,msg);
    exit(EXIT_FAILURE);
}

/**
 * Set global quit flag
 */
volatile sig_atomic_t quit = 0;

/**
 * Signal handler that catch singal
 */
static void handle_signal(int signal){
    quit = signal;
}

/**
 * Setting signal configuation
 */
static void signal_config(void){
    struct sigaction sa;
    sa.sa_handler = handle_signal;
    sa.sa_flags = 0;

    if(sigemptyset(&sa.sa_mask) < 0) {
        bail_out("sigemptyset");
    }

    
    const int signals[] = {SIGINT,SIGTERM};
    int k = (int) (sizeof(signals) / sizeof(signals[0]));
    for(int i = 0; i < k; i++) {
        if (sigaction(signals[i], &sa, NULL) < 0) {
            bail_out("sigaction err");
        }
    }

    
}

/**
 * Depending on number of clients we increase our array
 */
static void growArray(Client **client_arr, int c){
        Client *temp = (Client *) realloc(*client_arr,(c * sizeof(Client)));
        if(temp == NULL){
            bail_out("Error at realloc!\n");
        }
        *client_arr = temp;
}

sem_t *sem_server;
sem_t *sem_client;
sem_t *sem_global;

Client *client_list = NULL;


static void free_resources(void){

    strcpy(shared->server_response,"Server is about to close.");
    
    if(sem_close(sem_server) == -1){
		(void) fprintf(stderr, "Error at closing sem_server!\n");
	}
	
	if(sem_close(sem_client) == -1){
		(void) fprintf(stderr, "Error at closing  sem_client!\n");
	}

	if(sem_close(sem_global) == -1){
		(void) fprintf(stderr, "Error at closing  sem_global!\n");
    }
    
    if(munmap(shared,sizeof *shared) == -1){
        (void) fprintf(stderr,"Errot at munmap!\n");
    }

    if(shmfd != -1){
        if(shm_unlink(SHM_NAME) == -1){
            (void) fprintf(stderr,"Error at shm_unlink!\n");
        }
    }

    if(sem_unlink(SEM_SERVER) == -1){
        (void) fprintf(stderr,"Error at sem_unlink!\n");
    }

    if(sem_unlink(SEM_CLIENT) == -1){
        (void) fprintf(stderr,"Error at sem_unlink!\n");
    }

    if(sem_unlink(SEM_GLOBAL) == -1){
        (void) fprintf(stderr,"Error at sem_unlink!\n");
    }

    free(client_list);

    

}


/**
* @brief Program entry point.
* @param argc The argument counter.
* @param argv The argument vector.
* @return EXIT_FAILURE on failure or EXIT_SUCCESS on success.
*/
int main(int argc, char **argv){
    
    if(argc > 0){
        prog_name = argv[0];
    }

    if(argc > 1){
        usage();
    }

    if(atexit(free_resources) != 0){
        bail_out("atexit error");
    }

    (void) signal_config();

    shmfd = shm_open(SHM_NAME,O_RDWR | O_CREAT,PERMISSION);
    if(shmfd == -1){
        bail_out("Error open or create shared memory object");
    }

    if(ftruncate(shmfd, sizeof *shared) == -1){
        bail_out("Errot at truncate");
    }

    shared = mmap(NULL,sizeof *shared,PROT_READ | PROT_WRITE, MAP_SHARED,shmfd,0);

    if(shared == MAP_FAILED){
        bail_out("Memory mapping failed");
    }

    if(close(shmfd) == -1){
        bail_out("Error closing shmfd");
    }

    sem_server = sem_open(SEM_SERVER,O_CREAT | O_EXCL,PERMISSION,0);
    if(sem_server == SEM_FAILED){
        bail_out("Error creating server semaphore");
    }

    sem_client = sem_open(SEM_CLIENT,O_CREAT | O_EXCL,PERMISSION,0);
    if(sem_client == SEM_FAILED){
        bail_out("Error creating client semaphore");
    }

    sem_global = sem_open(SEM_GLOBAL,O_CREAT | O_EXCL,PERMISSION,0);
    if(sem_global == SEM_FAILED){
        bail_out("Error creating global semaphore");
    }
    int n = 1;
    char seq[MSG_LEN];
    client_list = (Client*) calloc(1,sizeof(Client));
    if(client_list == NULL){
        bail_out("Error at calloc call!\n");
    }

    if(sem_post(sem_client) == -1){
        bail_out("Error at sem_post client");
    }

    if(sem_post(sem_global) == -1){
        bail_out("Error at sem_post global");
    }
    
    while(!quit){

        printf("Server is waiting for requests!\n");

        if(sem_wait(sem_server) == -1){
            if(errno == EINTR){
                continue;
            } 
            bail_out("Error at sem_wait\n");
        }
        switch(shared->cmd){
            //new client 
            case(0): 
                global_id_counter++;
                shared->ID = global_id_counter;
                (void) bzero(shared->server_response, sizeof(shared->server_response));
                strcpy(shared->server_response,"You are now connected to server!");

                //first client so we dont need realloc
                if(n == 1){
                    client_list[0].ID = global_id_counter;
                    client_list[0].cursor = 0;
                    n++;
                }
                else{
                    (void) growArray(&client_list,n);
                    client_list[n-1].ID = global_id_counter;
                    client_list[n-1].cursor = 0;
                    n++;
                }
                break;
            // submit new seq
            case(1): ;
                int i=0;
                while(i<n){
                    if(client_list[i].ID == shared->ID){
                        strcpy(client_list[i].seqeunce,shared->seqeunce);
                        client_list[i].seq_saved = true; 
                        client_list[i].cursor = 0;
                        break;
                    }
                    i++;
                }
                (void) bzero(shared->server_response, sizeof(shared->server_response));
                strcpy(shared->server_response,"Sequence recieved by server!");
                break;
            // next protein seq (n)
            case(2): ;
                 i=0;
                bool startFound;
                bool stopFound;
                char protein[40];
                (void) bzero(protein, sizeof(protein));                
                int protein_pos[3];     // begin, end of protein and length of seq
                (void) bzero(protein_pos, sizeof(protein_pos));                                
                while(i<n){
                    if(client_list[i].ID == shared->ID){
                        // no seq found
                        if(!client_list[i].seq_saved){
                            (void) bzero(shared->server_response, sizeof(shared->server_response));
                            strcpy(shared->server_response,"Please enter the sequence first!");
                            break;
                        }
                        // try to find protein
                        else{
                            (void) bzero(seq, sizeof(seq));                                            
                            strcpy(seq,client_list[i].seqeunce);
                            int j = client_list[i].cursor;
                            //lenght of sequence
                            protein_pos[2] = strlen(seq);
                            // iterate through the whole seq and look up for proteins
                            for(; j<strlen(seq); j++){
                                // found start codon (M)
                                if(seq[j] == 'A' && seq[j+1] == 'U' && seq[j+2] == 'G'){
                                    if(!startFound){
                                        startFound = true;
                                        strcpy(protein,"M");
                                        // protein begin
                                        protein_pos[0] = j;
                                        j+=2;
                                    }
                                    else{
                                        strcat(protein,"M");  
                                        j+=2;                                      
                                    }
                                    stopFound = false;
                                }
                                /********************Start looking for protein sequence*********************/
                                else if(startFound){
                                    // found stop codon
                                     if((seq[j] == 'U' && seq[j+1] == 'A' && seq[j+2] == 'A') || (seq[j] == 'U' && seq[j+1] == 'A' && seq[j+2] == 'G') || (seq[j] == 'U' && seq[j+1] == 'G' && seq[j+2] == 'A')){
                                        stopFound = true;
                                        protein_pos[1] = j;
                                        /* if(protein_pos[0] > protein_pos[1]){
                                            protein_pos[1] = protein_pos[0];
                                        } */
                                        client_list[i].cursor = j+3;
                                        startFound = false;
                                        break;                                        
                                    }
                                    // found Phe(F)
                                    else if(seq[j] == 'U' && seq[j+1] == 'U' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"F");
                                        j+=2;
                                    }
                                    // found Leu(L)
                                    else if(seq[j] == 'U' && seq[j+1] == 'U' && (seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"L");
                                        j+=2;
                                    }
                                    // found Ser(S)
                                    else if(seq[j] == 'U' && seq[j+1] == 'C' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"S");
                                        j+=2;
                                    }
                                    // found Tyr(Y)
                                    else if(seq[j] == 'U' && seq[j+1] == 'A' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"Y");
                                        j+=2;
                                    }
                                     // found Cys(C)
                                    else if(seq[j] == 'U' && seq[j+1] == 'A' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"Y");
                                        j+=2;
                                    }
                                     // found Trp(W)
                                    else if(seq[j] == 'U' && seq[j+1] == 'G' &&  seq[j+2] == 'G'){
                                        strcat(protein,"W");
                                        j+=2;
                                    }
                                    // found Leu(L)
                                    else if(seq[j] == 'C' && seq[j+1] == 'U' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"L");
                                        j+=2;
                                    }
                                    // found Pro(P)
                                    else if(seq[j] == 'C' && seq[j+1] == 'C' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"P");
                                        j+=2;
                                    }
                                    // found His(H)
                                    else if(seq[j] == 'C' && seq[j+1] == 'A' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"H");
                                        j+=2;
                                    }
                                    // found Gln(Q)
                                    else if(seq[j] == 'C' && seq[j+1] == 'A' && (seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"Q");
                                        j+=2;
                                    }
                                    // found Arg(R)
                                    else if(seq[j] == 'C' && seq[j+1] == 'G' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"R");
                                        j+=2;
                                    }
                                     // found Ile(I)
                                    else if(seq[j] == 'A' && seq[j+1] == 'U' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A')){
                                        strcat(protein,"I");
                                        j+=2;
                                    }
                                    // found Thr(T)
                                    else if(seq[j] == 'A' && seq[j+1] == 'C' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"T");
                                        j+=2;
                                    }
                                    // found Asn(N)
                                    else if(seq[j] == 'A' && seq[j+1] == 'A' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"N");
                                        j+=2;
                                    }
                                    // found Lys(K)
                                    else if(seq[j] == 'A' && seq[j+1] == 'A' && (seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"K");
                                        j+=2;
                                    }
                                    // found Ser(S)
                                    else if(seq[j] == 'A' && seq[j+1] == 'G' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"S");
                                        j+=2;
                                    }
                                    // found Arg(R)
                                    else if(seq[j] == 'A' && seq[j+1] == 'G' && (seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"R");
                                        j+=2;
                                    }
                                    // found Asp(D)
                                    else if(seq[j] == 'G' && seq[j+1] == 'A' && (seq[j+2] == 'U' || seq[j+2] == 'C')){
                                        strcat(protein,"D");
                                        j+=2;
                                    }
                                    // found Glu(E)
                                    else if(seq[j] == 'G' && seq[j+1] == 'A' && (seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"E");
                                        j+=2;
                                    }
                                    // found Val(V)
                                    else if(seq[j] == 'G' && seq[j+1] == 'U' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"V");
                                        j+=2;
                                    }
                                    // found Ala(A)
                                    else if(seq[j] == 'G' && seq[j+1] == 'C' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"A");
                                        j+=2;
                                    }
                                    // found Gly(G)
                                    else if(seq[j] == 'G' && seq[j+1] == 'G' && (seq[j+2] == 'U' || seq[j+2] == 'C' || seq[j+2] == 'A' || seq[j+2] == 'G')){
                                        strcat(protein,"G");
                                        j+=2;
                                    }
                                    else{
                                        continue;
                                    }
                                }
                                /********************End looking for protein sequence*********************/
                            }
                            
                        }
                        //stop ID look up
                        break;
                    }
                    i++;
                }
                if(client_list[i].seq_saved){
                    if(protein[0] != 'M'){
                        (void) bzero(shared->server_response, sizeof(shared->server_response));
                        sprintf(shared->server_response,"End reached [%d/%d], send r to reset.",protein_pos[2],protein_pos[2]);
                    }
                    else if(!stopFound){
                        (void) bzero(shared->server_response, sizeof(shared->server_response));
                        sprintf(shared->server_response,"End reached [%d/%d], start codon found but not stop codon.",protein_pos[2],protein_pos[2]);
                    }
                    else{
                        (void) bzero(shared->server_response, sizeof(shared->server_response));
                        sprintf(shared->server_response,"Protein sequence found [%d/%d] to [%d/%d]: %s",protein_pos[0],protein_pos[2],protein_pos[1],protein_pos[2],protein);
                    }
                }
                break;
            // reset cursor (r)
            case(3):
                 i=0;
                while(i<n){
                    if(client_list[i].ID == shared->ID){                        
                        client_list[i].cursor = 0;
                        break;
                    }
                    i++;
                }
                if(!client_list[i].seq_saved){
                    (void) bzero(shared->server_response, sizeof(shared->server_response));
                    strcpy(shared->server_response,"Please enter the sequence first!");
                }
                else{
                    (void) bzero(shared->server_response, sizeof(shared->server_response));
                    sprintf(shared->server_response,"Reset. [0/%lu]",strlen(client_list[i].seqeunce));
                }
                break;
            // close client (q)
            case(4):
                 i=0;
                while(i<n){
                    if(client_list[i].ID == shared->ID){
                        (void) bzero(shared->server_response, sizeof(shared->server_response));
                        strcpy(shared->server_response,"Close client.");
                        break;
                    }
                    i++;
                }                
                break;
            default:
                (void) bzero(shared->server_response, sizeof(shared->server_response));
                strcpy(shared->server_response,"Unknown flow occured!");
                break;
        }

        if(sem_post(sem_client) == -1){
            bail_out("Error at sem_post!\n");
        }
    }
        
    if(quit != 0){
        free_resources();
        _exit(quit);
    }
    else{
        exit(EXIT_SUCCESS);
    }
    
}

/**
 * @file mrna-client.c
 * @author Alija-Ali Abadzic 01427575 <e01427575@student.tuwien.ac.at>
 * @date December 2017
 *
 * @brief Main program module.
 * 
 * Program for client-side of mrna porgram that read sequences
 * of clients mrna and commands that client sends and forward them to server.
 **/


#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <errno.h>
#include <stdbool.h>

/**
 * Name of shared memory object
 */
#define SHM_NAME "/01427575_mrna_shm"

/**
 * Permission for shm object
 */
#define PERMISSION (0600)

/**
 * Name of server semaphore 
 */
#define SEM_SERVER "/01427575_sem_server"

/**
 * Name of client semaphore
 */
#define SEM_CLIENT "/01427575_sem_client"

/**
 * Name of global control semaphore
 */
 #define SEM_GLOBAL "/01427575_sem_cntrl"

/**
 * Length of mrna that server receive from client
 */
#define MSG_LEN (100)

/**
 * Length of response that server sneds to client
 */
#define RES_LEN (200)

/**
 * Global variable
 * @brief Pointer to programm name
 */
char *prog_name = "<not_set>";


/**
 * Shared memory file descriptor
 */
static int shmfd;


/**
 * Struct used for share memory
 */
struct shm {

	// client ID
	int ID;

	// 0 - start/begin msg from client, 1 - submit new seq (s)
	// 2 - next protein seq (n)   3 - reset active seq (r) 
	// 4 - close client (q)
	int cmd;

	// sequence msg from client if cmd is 1 (s)
	char seqeunce[MSG_LEN];

	// protein sequence that has been found or other response depend. on cmd
	char server_response[RES_LEN];

};


static struct shm *shared;

/**
 * Mandatory usage function.
 * @brief This function writes helpful usage information about the program to stderr.
 * @details global variables: prog_name
 */
static void usage(){
	(void) fprintf(stderr,"USAGE: %s\n", prog_name);
	exit(EXIT_FAILURE);
}

/**
 * Print out error msg and exit programm with exit_failure
 */
static void bail_out(char *msg){
	(void)fprintf(stderr, "%s: %s\n",prog_name,msg);
	exit(EXIT_FAILURE);
}

/**
 * Set global quit flag
 */
volatile sig_atomic_t quit = 0;

/**
 * Signal handler that catch singal
 */
static void handle_signal(int signal){
	quit = signal;
}

/**
 * Setting signal configuation
 */
static void signal_config(void){
	struct sigaction sa;
	sa.sa_handler = handle_signal;
	sa.sa_flags = 0;

	if(sigemptyset(&sa.sa_mask) < 0) {
		bail_out("sigemptyset");
	}

	
	const int signals[] = {SIGINT,SIGTERM};
	int k = (int) (sizeof(signals) / sizeof(signals[0]));
	for(int i = 0; i < k; i++) {
		if (sigaction(signals[i], &sa, NULL) < 0) {
			bail_out("sigaction err");
		}
	}

	
}


sem_t *sem_server;
sem_t *sem_client;
sem_t *sem_global;


static void free_resources(void){

	if(sem_close(sem_server) == -1){
		(void) fprintf(stderr, "Error at closing sem_server!\n");
	}
	
	if(sem_close(sem_client) == -1){
		(void) fprintf(stderr, "Error at closing  sem_client!\n");
	}

	if(sem_close(sem_global) == -1){
		(void) fprintf(stderr, "Error at closing  sem_global!\n");
	}
	
	if(munmap(shared,sizeof *shared) == -1){
		(void) fprintf(stderr,"Errot at munmap!\n");
	}



}


/**
* @brief Program entry point.
* @param argc The argument counter.
* @param argv The argument vector.
* @return EXIT_FAILURE on failure or EXIT_SUCCESS on success.
*/
int main(int argc, char **argv){
	
	//int client_counter = 0;

	if(argc > 0){
		prog_name = argv[0];
	}

	if(argc > 1){
		usage();
	}

	if(atexit(free_resources) != 0){
		bail_out("atexit error");
	}

	(void) signal_config();

	shmfd = shm_open(SHM_NAME,O_RDWR | O_CREAT,PERMISSION);
	if(shmfd == -1){
		bail_out("Error open or create shared memory object");
	}

	if(ftruncate(shmfd, sizeof *shared) == -1){
		bail_out("Errot at truncate");
	}

	shared = mmap(NULL,sizeof *shared,PROT_READ | PROT_WRITE, MAP_SHARED,shmfd,0);

	if(shared == MAP_FAILED){
		bail_out("Memory mapping failed");
	}

	if(close(shmfd) == -1){
		bail_out("Error closing shmfd");
	}

	sem_server = sem_open(SEM_SERVER,0);
	if(sem_server == SEM_FAILED){
		bail_out("Error creating server semaphore");
	}

	sem_client = sem_open(SEM_CLIENT,0);
	if(sem_client == SEM_FAILED){
		bail_out("Error creating client semaphore");
	}

	sem_global = sem_open(SEM_GLOBAL,0);
	if(sem_global == SEM_FAILED){
		bail_out("Error creating global semaphore");
	}
	
	int cmd;
	char seq[MSG_LEN];
	int clientID = 0;
	bool IDset = false;
	printf("Available commands:\n");
	printf(" s - submit a new mRNA sequence\n");
	printf(" n - show next protein sequence in active mRNA sequence\n");
	printf(" r - reset active mRNA sequence\n");
	printf(" q - close this client\n");
	
	while(!quit) {
		if(!IDset){
			// "register" on server, to get the ID
			if(sem_wait(sem_global) == -1){
				if(errno == EINTR) continue;
				bail_out("Error at sem_wait client");
			}
			if(sem_wait(sem_client) == -1){
				if(errno == EINTR) continue;
				bail_out("Error at sem_wait global");
			}
			shared->cmd = 0;

			if(sem_post(sem_server) == -1){
				bail_out("Error at sem_post server");
			}

			if(sem_wait(sem_client) == -1){
				if(errno == EINTR) continue;
				bail_out("Error at sem_wait client");
			}

			(void) printf("%s\n",shared->server_response);

			IDset = true;
			clientID = shared->ID;

			printf("ID: %d\n",clientID);

			if(sem_post(sem_global) == -1){
				bail_out("Error at sem_post global");
			}
		
			if(sem_post(sem_client) == -1){
				bail_out("Error at sem_post client");
			}

		}

		else{
			bool endclient = false;
			bool wrongcmd = false;
			char cmdinput[1];
			(void) bzero(cmdinput,sizeof(cmdinput));
			int ch = 0;
			(void) printf("Enter new command. (Enter to end input): ");
			int k = 0;
			while (((ch = getchar()) != '\n') && ch != EOF ){
				cmdinput[0] = ch;
				k++;
			}
			// more than one cmd(char) typed
			if(k>1){
				(void) printf("Unknown command more than one cmd!\n");				
				wrongcmd = true;
			}
			else if(cmdinput[0] == 's'){
				int ch;
				int k = 0;
				int counter = 0;
				bool leastone;
				(void) bzero(seq,sizeof(seq));				
				(void) printf("Enter new mRNA sequence. (Newline to end input): ");
				while ( ((ch = getchar()) != EOF) && counter<2)  {
					if(ch == '\n'){
						 counter++;
					}
					if(k>=MSG_LEN || counter == 2){
						break;
					}
					if(ch == 'U' || ch == 'A' || ch == 'G' || ch == 'C'){
						seq[k] = ch;
						leastone = true;
					}
					k++;
				}
				if(!leastone){
					wrongcmd = true;
				}
				cmd = 1;
				//fgets(seq,MSG_LEN,stdin);	
			}
			else if(cmdinput[0] == 'n'){
				cmd = 2;
			}
			else if(cmdinput[0] == 'r'){
				cmd = 3;
			}
			else if(cmdinput[0] == 'q'){
				cmd = 4;
				endclient = true;
			}
			else{
				(void) printf("Unknown command!\n");
				wrongcmd = true;
			}
			

			if( (strcmp(shared->server_response,"Server is about to close.")) == 0){
				exit(EXIT_SUCCESS);
			}
			
			/* // check if server has closed shared memory object, exit on true
			shmfd = shm_open(SHM_NAME,O_EXCL | O_CREAT,PERMISSION);
			// no err means we could create it, so server is closed
			if(shmfd != -1){
				if(munmap(shared,sizeof *shared) == -1){
					(void) fprintf(stderr,"Errot at munmap!\n");
				}
			
				if(shm_unlink(SHM_NAME) == -1){
					(void) fprintf(stderr,"Error at shm_unlink!\n");
				}
				exit(EXIT_SUCCESS);
			} */

			if(wrongcmd == false){
				if(sem_wait(sem_global) == -1){
					if(errno == EINVAL) exit(EXIT_SUCCESS);
					if(errno == EINTR) continue;
					bail_out("Error at sem_wait client");
				}
				if(sem_wait(sem_client) == -1){
					if(errno == EINTR) continue;
					bail_out("Error at sem_wait global");
				}
				if(cmd == 1){
					strcpy(shared->seqeunce,seq);
				}
				shared->ID = clientID;
				shared->cmd = cmd;

				if(sem_post(sem_server) == -1){
					bail_out("Error at sem_post server");
				}

				if(sem_wait(sem_client) == -1){
					if(errno == EINTR) continue;
					bail_out("Error at sem_wait client");
				}

				(void) printf("%s\n",shared->server_response);

				if(sem_post(sem_global) == -1){
					bail_out("Error at sem_post global");
				}
			
				if(sem_post(sem_client) == -1){
					bail_out("Error at sem_post client");
				}
			}
			if(endclient){
				exit(EXIT_SUCCESS);
			}
		}
	}
		
	if(quit != 0){
		free_resources();
		_exit(quit);
	}
	else{
		exit(EXIT_SUCCESS);
	}
	
	
}






